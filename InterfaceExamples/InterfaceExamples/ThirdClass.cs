﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceExamples
{
    class ThirdClass : IFirst, ISecond
    {

        AClass obj1 = new AClass(); // instance of AClass
        BClass obj2 = new BClass(); // instance of BClass

        public void AMethod()  // implementing by using instance
        {
            obj1.AMethod();
        }

        public void BMethod() // implementing by using instance
        {
            obj2.BMethod();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceExamples
{
    class AClass : IFirst
    {
        public void AMethod()
        {
            Console.WriteLine("Method First");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceExamples
{
    class BClass : ISecond
    {
        public void BMethod()
        {
            Console.WriteLine("Method Second");
        }

    }
}

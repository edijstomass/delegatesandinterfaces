﻿using System;

namespace DelegatesWithReturnType
{
    public delegate int SampleDelegate();

    class Program
    {
        static void Main(string[] args)
        {


            SampleDelegate del = new SampleDelegate(SampleMethodOne);
            del += SampleMethodTwo; 
            int numbers = del();  // delegate saves only last number
            Console.WriteLine(numbers);


        }

        public static int SampleMethodOne()
        {
            return 1;
        }

        public static int SampleMethodTwo()
        {
            return 2;
        }


    }
}

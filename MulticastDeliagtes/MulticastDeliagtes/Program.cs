﻿using System;

namespace MulticastDeliagtes
{

    public delegate void SampleDelegate();

    class Program
    {
        static void Main(string[] args)
        {

            //SampleDelegate del1, del2, del3, del4;
            //del1 = new SampleDelegate(SampleMethodOne);
            //del2 = new SampleDelegate(SampleMethodTwo);
            //del3 = new SampleDelegate(SampleMethodThree);
            //del4 = del1 + del2 + del3 - del2; // this is called multicast delegate
            //del4(); 

            // SO VISU VAR IZDARIIT ARI IISAAK :

            SampleDelegate del = new SampleDelegate(SampleMethodOne);
            del += SampleMethodTwo;
            del += SampleMethodThree;   // if u wan't to remove method : del -= SampleMethodTwo;
            del();


        }

        public static void SampleMethodOne()
        {
            Console.WriteLine("Sample method ONE invoked");
        }

        public static void SampleMethodTwo()
        {
            Console.WriteLine("Sample method TWO invoked");
        }

        public static void SampleMethodThree()
        {
            Console.WriteLine("Sample method THREE invoked");
        }





    }
}

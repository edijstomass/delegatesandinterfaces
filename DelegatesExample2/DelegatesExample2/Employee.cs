﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DelegatesExample2
{

    delegate bool IsPromotable(Employee empl);

    class Employee
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public int Salary { get; set; }
        public int Experience { get; set; }


        public static void PromoteEmployee(List<Employee> employeeList, IsPromotable IsEligibleToPromote)
        {
            foreach(Employee employee in employeeList)
            {
                if(IsEligibleToPromote(employee))
                {
                    Console.WriteLine(employee.Name + " promoted!");
                }
                
            }
        }


    }
}

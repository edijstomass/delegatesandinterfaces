﻿using System;
using System.Collections.Generic;

namespace DelegatesExample2
{

 


    class Program
    {

        static void Main(string[] args)
        {

            List<Employee> empList = new List<Employee>();

            empList.Add(new Employee() { Name = "Edijs", Experience = 9, Id = 1, Salary = 1400 });
            empList.Add(new Employee() { Name = "Viktoria", Experience = 4, Id = 2, Salary = 1400 });
            empList.Add(new Employee() { Name = "Baiba", Experience = 6, Id = 3, Salary = 1400 });
            empList.Add(new Employee() { Name = "Annija", Experience = 3, Id = 4, Salary = 1400 });



            IsPromotable isPromotable = new IsPromotable(Promote);

            Employee.PromoteEmployee(empList, isPromotable);

        }


        public static bool Promote(Employee emp)
        {
            if(emp.Experience >= 5)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }
}

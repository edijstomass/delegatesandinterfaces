﻿using System;

namespace DelegatesExamples     // a delegate is a type safe function pointer
{

    // this delegate can point to any function,
    // that has void return type and takes string parameter
    public delegate void HelloFunctionDelegate(string Message); 


    class Program
    {
        static void Main(string[] args)
        {

            HelloFunctionDelegate del = new HelloFunctionDelegate(Hello); // needs to create a delegate instance
            del("Hello from delegate!!!");


        }



        public static void Hello(string strMessage)
        {
            Console.WriteLine(strMessage);
        }


    }
}

﻿using System;
using System.Collections.Generic;

namespace Example2WithLambda
{
    class Program
    {

        static void Main(string[] args)
        {

            List<Employee> empList = new List<Employee>();

            empList.Add(new Employee() { Name = "Edijs", Experience = 9, Id = 1, Salary = 1400 });
            empList.Add(new Employee() { Name = "Viktoria", Experience = 4, Id = 2, Salary = 1400 });
            empList.Add(new Employee() { Name = "Baiba", Experience = 6, Id = 3, Salary = 1400 });
            empList.Add(new Employee() { Name = "Annija", Experience = 7, Id = 4, Salary = 1400 });



            
            Employee.PromoteEmployee(empList, x => x.Experience >= 5);

        }



    }
}
